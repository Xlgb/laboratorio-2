
diccionario={702660977: "Axel Guzman Barahona",
             603213213: "Estrella Barahona Aguirre"}


def Ingresar_Usuario(ced , nom):
    '''La Opcion 1. Registrar datos de personas en un diccionario '''
    if ced in diccionario:
        return "El Usuario ya existe"
    diccionario[ced] = nom
    return "Usuario registrado con Exito"

def Recorrer_nombre():
    '''La Opcion 2. Recorrer los Nombres registrados en el diccionario '''
    return diccionario


def Eliminar_Usuario(ced):
    '''La Opcion 3.Eliminar una persona al ingresar la cedula'''
    if not ced in diccionario:
        return f"El numero de cedula {ced} no existe"
    if ced in diccionario:
        del diccionario[ced]
        return f"se eliminó con éxito {ced}"
def mostrar(ced):
    '''La Opcion 4.Consultar el nombre de una persona al registrar la cedula'''
    if not ced in diccionario:
        return "El numero de Cedula que Digitó no existe, Intente nuevamente"
    if ced in diccionario:
        print("\nEl nombre del Usuario encotrado es")
        return diccionario.get(ced)

def Cambiar_nombre(ced,nom):
    '''La Opcion 5.Cambiar el nombre ingresado una cedula por el usuario'''
    diccionario[ced] = nom
    return "Modificacion con existo"

def Documentacion():
    print("\n",Ingresar_Usuario.__doc__)
    print(Recorrer_nombre.__doc__)
    print(Eliminar_Usuario.__doc__)
    print(mostrar.__doc__)
    print(Cambiar_nombre.__doc__,"\n")





menu = ("1. Ingresar Usuarios\n"
        "2. Usuarios Regristrados\n"
        "3. Eliminar Usuarios\n"
        "4. Consultar Usuario\n"
        "5. Editar usuario\n"
        "0. Ver ducumentacion\n"
        "6. Salir\n"
        "\b Ingrese su Opcion :")
while True:
    op = int(input(menu))
    if op == 6:
        print("\nProceso Exitoso")
        break
    if op == 1:
        print()
        ced = int(input("Ingrese el numero de Cedula"))
        nom = (input("Ingrese el nombre del Usuario"))
        print("\n------",Ingresar_Usuario(ced,nom),"------\n")
    if op == 2:
        print("\n****** Lista de Clientes******\n",Recorrer_nombre(),"\n")
    if op == 3:
        ced = int(input("Ingrese el numero de cedula "))
        print("\n******",Eliminar_Usuario(ced),"******\n")
    if op == 4:
        ced = int(input("Ingrese el numero de Cedula"))
        print("\n******",mostrar(ced),"******\n")
    if op == 5:
        ced = int(input("Ingrese numero de cedula"))
        nom = input("Ingrese nombre nuevo")
        print(Cambiar_nombre(ced,nom))
    if op == 0:
        Documentacion()





